<?php

namespace Dionowl\Lucid\Providers;

use Dionowl\Lucid\Commands\MigrateCommand;
use Dionowl\Lucid\Commands\ModelCommand;
use Dionowl\Lucid\Commands\ResourceCommand;
use Illuminate\Support\ServiceProvider;

class LucidServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MigrateCommand::class,
                ModelCommand::class,
                ResourceCommand::class,
            ]);
        }
    }
}
